package controllers

import javax.inject._

import models._
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.i18n._
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

case class CreateUserForm(username: String, password: String)

class UserController @Inject()(repo: UserRepository,
                                 cc: MessagesControllerComponents
                                )(implicit ec: ExecutionContext)
  extends MessagesAbstractController(cc) {

  /**
   * The mapping for the person form.
   */
  val userForm: Form[CreateUserForm] = Form {
    mapping(
      "Username" -> nonEmptyText,
      "Password" -> nonEmptyText
    )(CreateUserForm.apply)(CreateUserForm.unapply)
  }

  /**
   * The index action.
   */
  def index = Action { implicit request =>
    Ok(views.html.index(userForm))
  }

  /**
   * The add person action.
   *
   * This is asynchronous, since we're invoking the asynchronous methods on PersonRepository.
   */
  def addUser = Action.async { implicit request =>

    userForm.bindFromRequest.fold(
      hasErrors = errorForm => {
        Future.successful(Ok(views.html.index(errorForm)))
      },
      success = user => {
        repo.create(user.username, user.password).map { _ =>
          Redirect(routes.UserController.index).flashing("success" -> "user.created")
        }
      }
    )
  }

  /**
   * A REST endpoint that gets all the people as JSON.
   */
  def getUsers = Action.async { implicit request =>
    repo.list().map { users =>
      Ok(Json.toJson(users))
    }
  }
}


